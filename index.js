import express from "express";
import mongoose from "mongoose";
import router from "./router.js";
import cors from "cors";
import fileUpload from "express-fileupload";

const PORT = process.env.PORT || 5000;
const DB_URL =
  process.env.Mongo_Connect_Url ||
  "mongodb+srv://adxamjon0806:VB0msb2pQlzwTvcV@mymongodb.jwiuk5b.mongodb.net/?retryWrites=true&w=majority";

const app = express();

app.use(express.json());
app.use(express.static("static"));
app.use(fileUpload({}));
app.use(
  cors({
    credentials: true,
    origin: "https://react-to-do-list-eosin-ten.vercel.app",
  })
);
app.use("", router);

const start = async () => {
  try {
    await mongoose.connect(DB_URL);
    app.listen(PORT, () => {
      console.log(`Server started on port: ${PORT}`);
    });
  } catch (e) {
    console.log(e);
  }
};

start();
